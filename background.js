/*
 * GLOBAL VARIABLES
 */
var DEBUG = 0;
var LOG = function() {
  if (DEBUG) {
    var args = ['download-blocker:'].concat(arguments);
    console.log.apply(console, args);
  }
};

/*
 * List of MIME types allowed to download.
 */
var ALLOWED_CONTENT_TYPES = {
  'text/plain': 'txt',
  'text/html': 'html',
  'application/pdf': 'pdf',
  'application/postscript': 'postscript'
};

function isRedirect(details) {
  return Math.floor(details.statusCode / 100) == 3;
};

/*
 * Extract Content-Type from <webRequest.HTTPHeaders>.
 */
function getContentType(headers) {
  for (var i = 0; i < headers.length; i++) {
    var header = headers[i];
    var name = header.name.toLowerCase();

    if (name == 'content-type') {
      // Trim suffixes after a semicolon. (e.g. this converts
      // 'plain/text; charset=utf-8' into 'plain/text')
      return header.value.split(';')[0].trim();
    }
  }
  return '';
};

/*
 * The callback function for <webRequest.onHeadersReceived>
 *
 * See the MDN manual on WebExtensions/webRequest.
 */
function onHeadersReceived(details) {
  LOG('onHeadersReceived ', JSON.stringify(details));
  var res = {};
  var headers = details.responseHeaders;

  // A "Content-Disposition" header will invoke "Save as File" handler
  // immediately, so we need to filter it from the header list.
  headers = headers.filter((header) => {
    return header.name.toLowerCase() != 'content-disposition';
  });

  // Redirect the HTTP request to void, unless the content type is
  // explicitly allowed.
  var contentType = getContentType(headers);
  var redirect = isRedirect(details);
  if (!redirect &&
      !ALLOWED_CONTENT_TYPES[contentType]) {
    res.redirectUrl = 'data:javascript,'
  }

  res.responseHeaders = headers;

  LOG('A request with Content-Type', contentType);
  LOG('Is redirection?', redirect);
  LOG('Return BlockingResponse', JSON.stringify(res));

  return res;
}

browser.webRequest.onHeadersReceived.addListener(
  onHeadersReceived,
  {urls: ['<all_urls>'], types: ['main_frame']},
  ['blocking', 'responseHeaders']
);

LOG('background script initialized')
